import java.util.ArrayList;

/**
 * Luokka mallintaa muistivihkoa.
 * Luokassa tallennetaan muistiinpanot muistivihkoon ja sieltä voidaan lukea.
 * Luokka myös palauttaa sen nimen ja omistajan.
 * @author Erkki
 */
public class Muistivihko {
    private String nimi;
    private String omistaja;
    private ArrayList<String> muistiinpanot;

    /**
     * Luo uuden olion.
     * @param nimi Asettaa oliolle nimen
     * @param omistaja Asettaa oliolle omistajan
     */
    public Muistivihko(String nimi, String omistaja) {
        this.nimi = nimi;
        this.omistaja = omistaja;
        muistiinpanot = new ArrayList<>();
    }

    /**
     * Palauttaa muistivihkon nimen
     * @return muistivihkon nimi
     */
    public String getNimi() {
        return nimi;
    }

    /**
     * Asettaa muistivihkolle uuden nimen
     * @param nimi muistivihkon uusi nimi
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    /**
     * palauttaa muistivihkon omistajan
     * @return omistajan nimi
     */
    public String getOmistaja() {
        return omistaja;
    }

    /**
     * asettaa uuden omistajan muistivihkolle
     * @param omistaja uuden omistajan nimi
     */
    public void setOmistaja(String omistaja) {
        this.omistaja = omistaja;
    }

    /**
     * lisaa uuden muistiinpanon muistivihkoon
     * @param viesti tallennettava viesti
     */
    public void lisaaMuistiinpano(String viesti) {
        this.muistiinpanot.add(viesti);
    }

    /**
     * palauttaa muistiinpanojen lukumäärän
     * @return muistiinpanojen määrä
     */
    public int muistiinpanoja() {
        return muistiinpanot.size();
    }

    /**
     * tulostaa kaikki muistivihkon muistiinpanot
     */
    public void tulostaMuistiinpanot() {
        for(String viesti : muistiinpanot) {
            System.out.println(viesti);
        }
    }
}
